# Geiser + Gerbil

This is a plugin for Geiser which adds rudimentary support for Gerbil Scheme.

## Limitations

1. No help available for native compiled functions.
2. No help available for macros.
3. Incomplete error highlight/jumping support.
4. ... and more

## Running

Geiser must already be installed before using `geiser-gerbil`. Loading
`geiser-gerbil.el` should be the only step necessary to add support for
Gerbil Scheme to geiser.

```
(load "/path/to/geiser-gerbil/elisp/geiser-gerbil.el")
```

It should now be possible to `M-x run-gerbil` to run Geiser with Gerbil.
